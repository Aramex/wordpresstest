<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wordpresstest' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[v%</fU/tFZAAgu;@mPzkV%)^#BMa:4#5d/y}lhcGU:6wJ*Na7p.Y~8b:W19Ow4T' );
define( 'SECURE_AUTH_KEY',  '0>a<,<ewW[}Id8s>oi *c.s025531xmzi,S%Q )T7?T<iIEP7q7!=smsNs1z0Z>u' );
define( 'LOGGED_IN_KEY',    'AlKaG ]O0 o);ee81<{{NY[1~E_yhZXC|_I]2*TsuRPV3OW}pLID2F{f,c,(< ^+' );
define( 'NONCE_KEY',        'GMpBM(A:wq)uH{u:o}RR4b;$Po0Z/5A_7z&3EWkE!OzG;lzL(YUt+we~6]Z>z2?G' );
define( 'AUTH_SALT',        '<iA/,Z;3eIY_5Y<)8FG9)UB*O hYFpQ~qjU! )?YxC8<o^TSn:e;Uy:$SUFJ5Tme' );
define( 'SECURE_AUTH_SALT', '#o^+I((EN?Tsga6eV;P$nGcEKf 1+?__!>kY*p71S)YGfz^~zna6`13wxCwKjCBX' );
define( 'LOGGED_IN_SALT',   '%Zta0/K,x[.Pqo_zC,I=XL&WWW{Ns0&NrBsG?9[u_bTDq&f^FS!U)=L]*F>B4i+D' );
define( 'NONCE_SALT',       '*%BW+c(-AR=oWK_)*T. ]6+5M]A-^P,*WoI*pCOA3pa>D6K#~1]?D In3l4^I^m7' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
